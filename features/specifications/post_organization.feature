#language: pt
@post_api
Funcionalidade: Post no endpoint Organization
  Realizar o cadastro de um novo elemento no endpoint Organization validando seus retornos

  Esquema do Cenário: Cadastro de novo elemento
    Dado que foi definido o endpoint "https://homologation-tax-admin.connection.nexaas.com/api/Organization"
    Quando realizo um post com um novo elemento
    Então a API deve retornar codigo 200
    E na resposta deve conter o mesmo valor do "<atributo>" que foi enviado na requisição
    Exemplos:
      | atributo              |
      | cnpj               |
      | inscricao_estadual |
      | razao_social       |


