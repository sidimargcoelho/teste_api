#language: pt
@get_api
Funcionalidade: Get no endpoint Organization
  Realizar requisições GET no Organization validando seus retornos

  Esquema do Cenário: Validação do retorno ao omitir parametros obrigatório
    Dado que foi definido o endpoint "https://homologation-tax-admin.connection.nexaas.com/api/Organization"
    Quando pesquiso passando "<cnpj>" e "<incricao_estadual>"
    Então a API deve retornar codigo 200
    E a mensagem "<retorno>"
    Exemplos:
      | cnpj | incricao_estadual | retorno                                 |
      | Sim  | Não               | 'Inscrição Estadual' deve ser informado |
      | Não  | Sim               | 'Cnpj' deve ser informado               |
      | Não  | Não               | 'Cnpj' deve ser informado               |
      | Não  | Não               | 'Inscrição Estadual' deve ser informado |

  Cenário: Validação do retorno ao enviar valores validos
    Dado que foi definido o endpoint "https://homologation-tax-admin.connection.nexaas.com/api/Organization"
    Quando pesquiso passando "<cnpj>" e "<incricao_estadual>"
    Então a API deve retornar codigo 200
    E na resposta deve conter o mesmo valor que foi enviado na requisição

  Esquema do Cenário: Validação do retorno ao enviar valores inválidos
    Dado que foi definido o endpoint "https://homologation-tax-admin.connection.nexaas.com/api/Organization"
    Quando pesquiso passando "<cnpj>" e\ou "<incricao_estadual>" inválido
    Então a API deve retornar codigo 200
    E a mensagem "<retorno>"
    Exemplos:
      | cnpj | incricao_estadual | retorno                                 |
      | Sim  | Não               | 'Inscrição Estadual' deve ser informado |
      | Não  | Sim               | 'Cnpj' deve ser informado               |
