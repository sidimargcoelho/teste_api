#language: pt
@patch_api
Funcionalidade: Patch no endpoint Organization
  Realizar a alteração parcial de um elemento no endpoint Organization validando seus retornos

  Esquema do Cenário: Alteração de um elemento
    Dado que foi definido o endpoint "https://homologation-tax-admin.connection.nexaas.com/api/Organization"
    Quando realizo um PATCH alternado o "<atributo>" do elemento elemento
    Então a API deve retornar codigo 200
    E na resposta deve conter o valor alterado do "<atributo>" que foi enviado na requisição
    Exemplos:
      | atributo     |
      | razao_social |


