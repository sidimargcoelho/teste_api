require 'factory_bot'
require 'cucumber'
require 'faker'
require 'httparty'
require 'rspec'
require 'json'

require_relative 'config/spec_rest'

VARIABLE = YAML.load_file(File.dirname(__FILE__) + "/values.yaml")

World(REST, FactoryBot::Syntax::Methods)