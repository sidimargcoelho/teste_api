class GetApi
  include HTTParty
  include RSpec::Matchers

  def get_organization(cnpj, ie)
    if cnpj == 'Sim' && ie == 'Não'
      # $response = organization.get_organization VARIABLE['cnpj'], nil
      HTTParty.get($uri_base, query: { cnpj: VARIABLE['cnpj'] })
    elsif cnpj == 'Não' && ie == 'Sim'
      HTTParty.get($uri_base, query: { ie: VARIABLE['inscricao_estadual'] })
    elsif cnpj == 'Não' && ie == 'Não'
      HTTParty.get($uri_base)
    else
      HTTParty.get($uri_base, query: { cnpj: VARIABLE['cnpj'], ie: VARIABLE['inscricao_estadual'] })
    end
  end

  def get_organization_fake(cnpj, ie)
    cnpj_vale = Faker::Company.brazilian_company_number
    ie_value = Faker::Number.number(digits: 12)
    if cnpj == 'Sim' && ie == 'Não'
      $response = HTTParty.get($uri_base, query: { cnpj: cnpj_vale })
    elsif cnpj == 'Não' && ie == 'Sim'
      $response = HTTParty.get($uri_base, query: { ie: ie_value })
    end
  end

  def valid_get_message(message)
    expect($response["messages"]).to include(message)
  end

  def valid_get_organization(atributes,value)
    expect($response["organization"][atributes]).to eq(value)
  end

end
