
class PatchApi
  include HTTParty
  include RSpec::Matchers

  def gen_update_value(atributo)
    code_id = Faker::Number.number(digits: 4)
    @update_value = {
      "cnpj": "57372460845206",
      "inscricao_estadual": "426736220755",
      "razao_social": Faker::Company.name,
      "invoice_system_id": 39,
      "invoice_system_credentials": {
        "Token": "6a381d6862ac4fbe951c5ac5f1caf113",
        "OrganizationId": code_id.to_s
      },
      "calculation_engine_id": 39,
      "calculation_engine_credentials": {
        "Token": "6a381d6862ac4fbe951c5ac5f1caf113",
        "OrganizationId": code_id.to_s
      },
      "external_code": "1072024",
      "address": {
        "zip_code": "04552-000",
        "ibge_code": "3550308",
        "neighborhood": "Vila Olímpia",
        "street": "R. do Rocio",
        "number": 199,
        "state": "São Paulo",
        "city": "São Paulo",
        "detail": "Predio residencial"
      },
      "callbacks": [{
                      "callback_url": "https://www.cypress-webhook.com.br",
                      "description": "Cypress Test",
                      "callback_types": [1, 2]
                    }],
      "emails": [{
                   "email": "lucas.duarte@nexaas.com",
                   "description": "Testador"
                 }],
      "invoice_dispatching_frequency": "monthly",
      "series": [{
                   "id": 1,
                   "number": "402",
                   "invoiceType": "NFCE",
                   "lastInvoiceNumber": 10035
                 }],
      "fisco_messages": [{

                           "operation": "Sell",
                           "invoice_type": "NFE",
                           "message": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quam justo, molestie at vehicula sit amet, aliquet ut dolor."
                         },
                         {
                           "operation": "Sell",
                           "invoice_type": "NFCE",
                           "message": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quam justo, molestie at vehicula sit amet, aliquet ut dolor."
                         },
                         {
                           "operation": "Devolution",
                           "invoice_type": "NFE",
                           "message": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quam justo, molestie at vehicula sit amet, aliquet ut dolor."
                         },
                         {
                           "operation": "Transfer",
                           "invoice_type": "NFE",
                           "message": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quam justo, molestie at vehicula sit amet, aliquet ut dolor."
                         }]
    }
    @update_value.to_json
  end

  def patch_organization(value)
    HTTParty.patch($uri_base,:headers => { 'Content-Type' => 'application/json' }, :body => value)
  end

end
