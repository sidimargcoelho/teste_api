class ApiHelper
  include HTTParty
  include RSpec::Matchers

  def valid_response(value)
    expect($response.code).to eq(value)
  end

  def valid_organization(atributes, object)
    value = JSON.parse(object)
    expect($response["organization"][atributes]).to eq(value[atributes])
  end


end
