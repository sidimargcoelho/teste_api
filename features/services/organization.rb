module Rest 
    class Organization
        include HTTParty 
        
        headers 'Content-Type' => 'application/json'

        def get_organization(cnpj,ie)
            self.class.get($uri_base, query: { cnpj: cnpj, ie: ie })
        end

        def post_organization(organization)
            self.class.post($uri_base,:headers => { 'Content-Type' => 'application/json' }, body: organization.to_json)
        end
    end    
end