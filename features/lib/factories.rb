require 'faker'
require_relative '../models/organization'

FactoryBot.define do
    factory :organization, class: OrganizationModel do
        cnpj {Faker::Company.brazilian_company_number}
        inscricao_estadual {Faker::Number.number(digits: 12)}
        razao_social {Faker::Company.name}
    end
end 