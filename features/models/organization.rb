class OrganizationModel
    attr_accessor :cnpj, :inscricao_estadual, :razao_social, :invoice_system_credentials

    def organization_hash
        gen_organization_id = Faker::Number.number(digits: 3)
        {
          cnpj: @cnpj,
          inscricao_estadual: @inscricao_estadual,
          razao_social: @razao_social,
          "invoice_system_id": 39,
          "invoice_system_credentials": {
            "Token": "6a381d6862ac4fbe951c5ac5f1caf113",
            "OrganizationId": gen_organization_id.to_s
          },
          "calculation_engine_id": 39,
          "calculation_engine_credentials": {
            "Token": "6a381d6862ac4fbe951c5ac5f1caf113",
            "OrganizationId": gen_organization_id.to_s
          }
         }
        end
    end