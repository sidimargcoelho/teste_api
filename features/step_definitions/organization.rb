Dado('que foi definido o endpoint {string}') do |endpoint|
  $uri_base = endpoint
end

Quando('pesquiso passando {string} e {string}') do |cnpj, ie|
  $response = GetApi.new.get_organization(cnpj, ie)
end

Quando('realizo um PATCH alternado o {string} do elemento elemento') do |atributo|
  @update_value = PatchApi.new.gen_update_value atributo
  @payload_organization = build(:organization).organization_hash
  puts @update_value
  puts @payload_organization.to_json
  $response = PatchApi.new.patch_organization @update_value
end

Quando('realizo um post com um novo elemento') do
  @new_value = PostApi.new.gen_value
  $response = PostApi.new.post_organization @new_value
end

Quando(/^pesquiso passando "([^"]*)" e\\ou "([^"]*)" inválido$/) do |cnpj, ie|
  GetApi.new.get_organization_fake(cnpj, ie)
end

Então('a mensagem {string}') do |message|
  ApiHelper.new.valid_response 200
  GetApi.new.valid_get_message(message)
end

Então('a API deve retornar codigo {int}') do |code_response|
  ApiHelper.new.valid_response code_response
end

Então('na resposta deve conter o mesmo valor do {string} que foi enviado na requisição') do |atributo|
  ApiHelper.new.valid_organization(atributo, @new_value)
end

Então('na resposta deve conter o valor alterado do {string} que foi enviado na requisição') do |atributo|
  ApiHelper.new.valid_organization(atributo, @update_value)
end

Então('na resposta deve conter o mesmo valor que foi enviado na requisição') do
  GetApi.new.valid_get_organization("cnpj", VARIABLE['cnpj'])
  GetApi.new.valid_get_organization("inscricao_estadual", VARIABLE['inscricao_estadual'])
end

